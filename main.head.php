<?php 
include_once('./head.php'); 
?>
  <div class="all">
    <div class="head">
      <div class="top">
        <div style="flex:1;"></div>
          <div class="logo">
            <img src="./images/header/logo.png"/>
          </div>
          <div class="logo-copy">
            FIND A UNIQUE PLACE
          </div>
          <div class="login">
            <a href="./login.php">로그인</a>
          </div>
        <div style="flex:1;"></div>
      </div>
      <div class="menu">
        <div style="flex:1;"></div>
        <div style="flex:1; display: flex;">
          <div class="rank">
            <a href="./rank.php">랭킹</a>
          </div>
          <div class="news">
            <a href="./news.php">매거진</a>
          </div>
        </div>
        <div class="hot-search">
          <div class="hot-word">
            인기검색어
          </div>
          <div class="hot-rank">
            <div style="flex:0 1 auto;">
              <div class="hot">
                <div class="hot-list-h"><div class="num">1</div><a href="#">강남 맛집 찾기</a></div>
                <div class="hot-list" style="display:none;"><div class="num">2</div><a href="#">서초 맛집 찾기</a></div>
                <div class="hot-list" style="display:none;"><div class="num">3</div><a href="#">구의 맛집 찾기</a></div>
                <div class="hot-list" style="display:none;"><div class="num">4</div><a href="#">건대 맛집 찾기</a></div>
                <div class="hot-list" style="display:none;"><div class="num">5</div><a href="#">목동 맛집 찾기</a></div>
                <div class="hot-list" style="display:none;"><div class="num">6</div><a href="#">잠실 맛집 찾기</a></div>
                <div class="hot-list" style="display:none;"><div class="num">7</div><a href="#">신림 맛집 찾기</a></div>
                <div class="hot-list" style="display:none;"><div class="num">8</div><a href="#">강변 맛집 찾기</a></div>
                <div class="hot-list" style="display:none;"><div class="num">9</div><a href="#">역삼 맛집 찾기</a></div>
                <div class="hot-list" style="display:none;"><div class="num">10</div><a href="#">논현 맛집 찾기</a></div>
              </div>
            </div>
          </div>
          <div class="rank_btn">
            <img style="display:block; padding: 5px;" id="down_bt" src="./images/rank_down.png" />
            <img style="display:none; padding: 5px;" id="up_bt" src="./images/rank_up.png" />
          </div>
        </div>
        <div style="flex:1;"></div>
      </div>
    </div>  